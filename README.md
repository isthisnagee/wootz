Wootz: A Static Site Tool and Generator
=======================================

Wootz is a static site command line interface.

**This tool is currently in development**. For now it's just a tool to help me learn Rust :)

- [@isthisnagee](https://twitter.com/isthisnagee)


Usage
-----

```bash
$ wootz new site-name
wootz: created folder `site-name`
wootz: added folder `site-name/src`
wootz: added file `site-name/src/index.md`
$ cd site-name
$ wootz page about
wootz: added page `about`. File in `site-name/src/pages/about.md`
$ wootz page 404
wootz: added page `404`. File in `site-name/src/pages/404.md`
$ wootz section blog
wootz: added section `blog`. Folder in `site-name/src/blog`
$ wootz page --section blog first-topic
wootz: added page `404` to section `blog`. File in `site-name/src/blog/pages/404.md`
$ wootz build
wootz: Build!. HTML files located in `site-name/dist`
$ wootz build --dir docs
wootz: Build!. HTML files located in `site-name/docs`
$ wootz watch
Currently Watching! Saving a file in `src` will automatically refresh the page!
👀
...
```

