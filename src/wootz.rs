#[cfg(test)]
mod tests {
    use std::collections::HashSet;

    #[test]
    fn config_add_section() {
        let location = String::from("path");
        let dot = Config::new(location);
        let empty_hash: HashSet<String> = HashSet::new();
        assert_eq!(dot.root_path, "path");
        assert_eq!(dot.relative_index_path, String::from("/src/index.md"));
        assert_eq!(dot.sections, empty_hash);
    }

    #[test]
    fn config_new() {
        let mut dot = Config::new(String::from("path"));
        assert_eq!(dot.sections, HashSet::new());

        let new_section = String::from("new_section");
        dot.add_section(new_section);
        assert!(dot.sections.contains("new_section"));
        assert!(dot.sections.len() == 1);
    }

}

use std::collections::HashSet;

#[derive(Clone, Deserialize, Serialize)]
pub struct Config {
    root_path: String,
    relative_index_path: String,
    sections: HashSet<String>,
}

impl Config {
    pub fn add_section(&mut self, section: String) {
        self.sections.insert(section);
    }
    pub fn new(path: String) -> Config {
        let config = Config {
            root_path: path.clone(),
            relative_index_path: String::from("/src/index.md"),
            sections: HashSet::new(),
        };
        return config;
    }
}
