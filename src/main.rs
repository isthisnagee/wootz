extern crate clap;
extern crate serde;
#[macro_use]
extern crate serde_derive;
extern crate toml;

use std::fs::{DirBuilder, File};
use std::io::prelude::*;

use toml::to_string_pretty;
use clap::{App, Arg, ArgMatches, SubCommand};

mod wootz;
use wootz::Config;

fn write(path: String, content: &[u8]) -> Result<(), std::io::Error> {
    let mut f = File::create(path.clone())?;
    f.write_all(content)?;
    return Ok(());
}

fn main() {
    let matches = App::new("wootz")
        .version("0.0.1")
        .about("a static site generator")
        .author("isthisnagee <isthisnagee@gmail.com>")
        .subcommand(
            SubCommand::with_name("new")
                .about("create a new project")
                .author("isthisnagee <isthisnagee@gmail.com>")
                .arg(
                    Arg::with_name("dir")
                        .help("the name of the directory to create")
                        .required(true),
                )
                .arg(Arg::from_usage(
                    "-f, --force... 'forcefully create the directory'",
                )),
        )
        .subcommand(
            SubCommand::with_name("page")
                .about("Create a new page, optionally specifying a section")
                .author("isthisnagee <isthisnagee@gmail.com>")
                .arg(
                    Arg::with_name("name")
                        .help("the name of the page")
                        .required(true),
                )
                .arg(
                    Arg::from_usage("--section <SECTION> 'the section to add the page to'")
                        .required(false),
                ),
        )
        .get_matches();

    match matches.subcommand() {
        ("new", Some(new_matches)) => new_wootz(new_matches),
        ("page", Some(page_matches)) => if_wootz_then(&new_page, page_matches),
        ("section", Some(section_matches)) => if_wootz_then(&new_section, section_matches),
        _ => unreachable!(),
    }
}

fn if_wootz_then(fun: &Fn(&ArgMatches) -> (), args: &ArgMatches) {
    if std::fs::metadata(".wootz").unwrap().is_file() {
        fun(args);
    }
    // TODO: err msg
}

fn new_wootz(args: &ArgMatches) {
    let dirname = args.value_of("dir").unwrap();
    if std::fs::metadata(dirname).unwrap().is_dir() {
        // TODO: err msg
        return;
    }
    DirBuilder::new().create(dirname).unwrap();
    println!("'wootz new {}' called", args.value_of("dir").unwrap());

    let dotfile_name = String::from(".wootz");
    let mut toml = Config::new(dotfile_name.clone());
    toml.add_section(String::from("new_section"));

    let toml_s = to_string_pretty(&toml).unwrap();
    let ok = write(dotfile_name.clone(), toml_s.into_bytes().as_slice());
    match ok {
        Ok(_) => println!("Success!"),
        Err(e) => eprintln!("{}", e),
    }
}

fn new_page(args: &ArgMatches) {
    println!("'wootz page {}' called", args.value_of("name").unwrap());
}

fn new_section(args: &ArgMatches) {
    println!("'wootz section {}' called", args.value_of("name").unwrap());
}
